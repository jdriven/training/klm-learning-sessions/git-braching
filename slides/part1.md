# Learning session 
### Branching in Git    

---slide---

## What is Git?

![cloud](slides/images/cloud.png)

---vertical---

- CLI
- GUI
- IDE plugins
- ...

---slide---

## Mastering Git Branching

![branches](slides/images/branches.png)

Note:
- discuss branching
- tutorial
- discussion

---slide---

### Committing changes

![commit](slides/images/commit.png)

Note:
A commit contains changes you made and saved to git

---vertical---

### Amend commits

![commit](slides/images/commit.png)
![amend](slides/images/amend.png)

Note:
- forgot changes
- used wrong credentials
- found out your code didnt compile

---slide---

### Ignoring files

![gitignore](slides/images/git-not-ignore.png)

Note:
.gitignore in the root of git repo

---vertical---

### Ignoring files

![gitignore](slides/images/gitignore.png)

Tip:
```
touch ~/.gitignore_global
git config --global core.excludesFile '~/.gitignore'
```

Note:
files you don't want to track:
- generated code
- IDE folders / setup

do track:
- code
- code style/formatting options

---slide---

### Comparing versions

![diff](slides/images/diff.png)

Tip: create a _compare-branch_ hotkey in your IDE!

---slide---

### Branching

![branching](slides/images/branching.png)

Note:
- undisturbed work
- branch releases for testing
- tracking progress on a feature

---vertical---

### Merging

![merging](slides/images/merging.png)

Note:
- for others
- to get your feature released
- to get other's work

Easy to perform, conflicts per merge

---vertical---

### Rebasing

![rebasing](slides/images/rebasing.png)

N.B. Rebasing:
- changes history
- can be done interactively

Note:
Same as merging, but linear history
Recommended to keep history clear.
Harder to perform, conflicts per commit

---slide---

### Resolving conflicts

![conflict](slides/images/conflict1.png)


---vertical---

### Resolving conflicts

![conflict](slides/images/conflict2.png)

Note:
GUI will help a lot

---slide---

### Squashing commits

![unsquashed](slides/images/unsquashed.png)

---vertical---

### Squashing commits

![squashing](slides/images/squashing.png)

N.B. squashing is an interactive rebase!

`git squash` does not even exist :)  

---vertical---

### Squashing commits

![squashed](slides/images/squashed.png)

N.B. Squashing commits changes history!

---slide---

### Keep your work, forever!

![squashed](slides/images/remote.png)

N.B:
When history was changed, use the force.

Note:
- Protect you branches
- Only force your own branches

---slide---

## Tutorial 

Master this yourself

https://learngitbranching.js.org/

Note:

https://learngitbranching.js.org/?NODEMO

```
reset;
git clone;
git checkout main;git commit;git commit;git checkout -b feature1 o/main;git commit;git commit;git checkout -b feature2 o/main;git commit;git commit;git checkout -b feature3 o/main;

git merge main;
git checkout feature1;
git commit;
git commit --amend;
git merge feature2;
git rebase main;
git rebase feature1 main;
git rebase -i C1 main;
git push;

git branch -d feature1 feature2 feature3;
```
---slide---

